package com.ConstructionSite.service;

import com.yy.commons.mybatis.service.BaseService;
import com.ConstructionSite.entity.OssEntity;
import com.yy.commons.tools.page.PageData;

import java.util.Map;

/**
 * 文件上传
 *
 * @author shelei
 */
public interface OssService extends BaseService<OssEntity> {

	PageData<OssEntity> page(Map<String, Object> params);
}
