package com.constructionsite.mapper.securityDataMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.constructionsite.entity.SecurityDataEntity.SecurityData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecurityDataMapper extends BaseMapper<SecurityData> {
}
