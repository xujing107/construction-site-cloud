package com.constructionsite.mapper.securityMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.constructionsite.entity.securityEntity.SecuritySchedule;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecurityScheduleMapper extends BaseMapper<SecuritySchedule> {
}
