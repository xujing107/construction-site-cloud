package com.constructionsite.service.safetyCostService;


import com.baomidou.mybatisplus.extension.service.IService;
import com.constructionsite.entity.SafetyCostEntity.SafetyCost;

public interface SafetyCostService extends IService<SafetyCost> {
}
