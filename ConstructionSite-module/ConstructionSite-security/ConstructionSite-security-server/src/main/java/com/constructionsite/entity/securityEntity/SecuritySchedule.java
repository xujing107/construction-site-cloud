package com.constructionsite.entity.securityEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

//安全日程
@Data
@ApiModel("安全日程")
@TableName("t_SecuritySchedule")
public class SecuritySchedule {

    @ApiModelProperty("日程id")
    @TableField("schedule_id")
    private Integer scheduleId;

    @ApiModelProperty("日程")
    @TableField("schedule")
    private String schedule;

    @ApiModelProperty("类型")
    @TableField("type")
    private String type;

    @ApiModelProperty("地点")
    @TableField("addrss")
    private String addrss;//地点

    @ApiModelProperty("开始时间")
    @TableField("start_time")
    private Date startTime;

    @ApiModelProperty("结束时间")
    @TableField("end_time")
    private Date endTime;

    @ApiModelProperty("参与人员")
    @TableField("participants")
    private String participants;//参与人员

}
