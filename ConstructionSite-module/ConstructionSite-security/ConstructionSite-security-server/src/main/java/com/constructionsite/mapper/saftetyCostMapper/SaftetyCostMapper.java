package com.constructionsite.mapper.saftetyCostMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.constructionsite.entity.SafetyCostEntity.SafetyCost;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SaftetyCostMapper extends BaseMapper<SafetyCost> {
}
