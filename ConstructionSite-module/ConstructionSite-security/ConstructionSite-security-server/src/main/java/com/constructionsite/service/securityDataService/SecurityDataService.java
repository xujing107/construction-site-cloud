package com.constructionsite.service.securityDataService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.constructionsite.entity.SecurityDataEntity.SecurityData;

public interface SecurityDataService extends IService<SecurityData> {
}
