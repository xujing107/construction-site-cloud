package com.constructionsite.mapper.riskMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.constructionsite.entity.riskEntity.Risk;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RiskMapper extends BaseMapper<Risk> {
}
