package com.constructionsite.service.securityService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.constructionsite.entity.securityEntity.SecuritySchedule;
import com.yy.commons.tools.utils.PageUtils;

import java.util.Map;

public interface SecurityScheduleService extends IService<SecuritySchedule> {

    PageUtils queryAll(Map<String,Object> map);

    void add(SecuritySchedule securitySchedule);
}
