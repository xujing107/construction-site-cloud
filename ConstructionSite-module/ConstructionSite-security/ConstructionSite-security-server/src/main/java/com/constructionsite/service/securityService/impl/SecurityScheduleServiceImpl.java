package com.constructionsite.service.securityService.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.constructionsite.entity.securityEntity.SecuritySchedule;
import com.constructionsite.mapper.securityMapper.SecurityScheduleMapper;
import com.constructionsite.service.securityService.SecurityScheduleService;
import com.yy.commons.tools.utils.PageUtils;
import com.yy.commons.tools.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SecurityScheduleServiceImpl extends ServiceImpl<SecurityScheduleMapper, SecuritySchedule> implements SecurityScheduleService {


    @Override
    public PageUtils queryAll(Map<String, Object> map) {

        IPage<SecuritySchedule> iPage = this.page(new Query<SecuritySchedule>().getPage(map));

        PageUtils pageUtils = new PageUtils(iPage);

        return pageUtils;
    }

    @Override
    public void add(SecuritySchedule securitySchedule) {

        int insert = this.baseMapper.insert(securitySchedule);
    }
}
