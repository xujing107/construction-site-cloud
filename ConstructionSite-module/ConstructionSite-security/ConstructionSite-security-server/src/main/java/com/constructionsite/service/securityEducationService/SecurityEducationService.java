package com.constructionsite.service.securityEducationService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.constructionsite.entity.securityEducationEntity.SecurityEducation;

public interface SecurityEducationService extends IService<SecurityEducation> {
}
