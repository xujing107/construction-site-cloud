package com.constructionsite.controller;

import com.constructionsite.service.securityService.SecurityScheduleService;
import com.yy.commons.tools.utils.PageUtils;
import com.yy.commons.tools.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

@RestController
@Api(tags = "安全日程")
public class SecurityScheduleController {

    @Autowired
    private SecurityScheduleService securityScheduleService;

    @GetMapping("/queryAll")
    @ApiOperation("查询所有日程信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value="当前页",name="page",dataType="Stirng",paramType = "query",required = false),
            @ApiImplicitParam(value="一页显示条数",name="limit",dataType="String",paramType = "query",required = false),
            @ApiImplicitParam(value="排序-asc升序，desc降序",name="order",dataType="Stirng",paramType = "query",required = false),
            @ApiImplicitParam(value="根据什么排序",name="sidx",dataType="Stirng",paramType = "query",required = false),
    })
    public Result<PageUtils> list(@RequestParam @ApiIgnore Map<String,Object> map){

        try {
            PageUtils pageUtils = this.securityScheduleService.queryAll(map);

            return new Result<PageUtils>().ok(pageUtils);
        }catch (Exception e){
            e.printStackTrace();

            return new Result<PageUtils>().error("查询失败");
        }
    }
}
