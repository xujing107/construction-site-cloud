package com.constructionsite.mapper.securityEducationMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.constructionsite.entity.securityEducationEntity.SecurityEducation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecurityEducationMapper extends BaseMapper<SecurityEducation> {
}
