package com.constructionsite.mapper.emergencyMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.constructionsite.entity.emergencyEntity.Emergency;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmergencyMapper extends BaseMapper<Emergency> {
}
