package com.constructionsite.service.riskService;


import com.baomidou.mybatisplus.extension.service.IService;
import com.constructionsite.entity.riskEntity.Risk;

public interface RiskService extends IService<Risk> {
}
